<?php

namespace Linkmark\GuzzleBundle;

use Linkmark\GuzzleBundle\DependencyInjection\Compiler\ServicesCompilerPass;
use Linkmark\GuzzleBundle\DependencyInjection\Compiler\SubscribersCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * {@inheritdoc}
 */
class LinkmarkGuzzleBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new SubscribersCompilerPass());
        $container->addCompilerPass(new ServicesCompilerPass());
    }
}
