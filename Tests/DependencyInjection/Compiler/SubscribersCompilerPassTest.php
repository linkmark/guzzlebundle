<?php

namespace Linkmark\GuzzleBundle\Tests\DependencyInjection\Compiler;

use GuzzleHttp\Message\Response;
use GuzzleHttp\Subscriber\Mock;
use Linkmark\GuzzleBundle\DependencyInjection\Compiler\SubscribersCompilerPass;
use Linkmark\GuzzleBundle\DependencyInjection\LinkmarkGuzzleExtension;
use PHPUnit_Framework_TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * @coversDefaultClass \Linkmark\GuzzleBundle\DependencyInjection\Compiler\SubscribersCompilerPass
 */
class SubscribersCompilerPassTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerBuilder
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->container = new ContainerBuilder();
        $extension = new LinkmarkGuzzleExtension();

        $this->container->registerExtension($extension);
        $this->container->loadFromExtension($extension->getAlias());
    }

    /**
     * @covers ::process
     */
    public function testProcess()
    {
        $container = $this->container;

        $provider = new Definition(
            'GuzzleHttp\\Subscriber\\Mock',
            [
                [
                    new Response(200),
                    new Response(202),
                ],
            ]
        );
        $provider->addTag('linkmark_guzzle.subscriber');

        $container->setDefinition('acme.guzzle.custom_subscriber', $provider);

        $container->addCompilerPass(new SubscribersCompilerPass());
        $container->compile();

        $this->assertTrue($container->has('linkmark_guzzle'));
        $this->assertTrue($container->has('acme.guzzle.custom_subscriber'));

        $guzzle = $container->get('linkmark_guzzle');

        $this->assertEquals(200, $guzzle->get('/')->getStatusCode());
        $this->assertEquals(202, $guzzle->get('/')->getStatusCode());
    }
}
