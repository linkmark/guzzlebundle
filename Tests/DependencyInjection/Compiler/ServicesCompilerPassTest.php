<?php

namespace Linkmark\GuzzleBundle\Tests\DependencyInjection\Compiler;

use Linkmark\GuzzleBundle\DependencyInjection\Compiler\ServicesCompilerPass;
use Linkmark\GuzzleBundle\DependencyInjection\LinkmarkGuzzleExtension;
use PHPUnit_Framework_TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * @coversDefaultClass \Linkmark\GuzzleBundle\DependencyInjection\Compiler\ServicesCompilerPass
 */
class ServicesCompilerPassTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerBuilder
     */
    private $container;

    private $description = [
        'baseUrl' => 'http://httpbin.org/',
        'operations' => [
            'testing' => [
                'httpMethod' => 'GET',
                'uri' => '/get/{foo}',
                'responseModel' => 'getResponse',
                'parameters' => [
                    'foo' => [
                        'type' => 'string',
                        'location' => 'uri',
                    ],
                    'bar' => [
                        'type' => 'string',
                        'location' => 'query',
                    ],
                ],
            ],
        ],
        'models' => [
            'getResponse' => [
                'type' => 'object',
                'additionalProperties' => [
                    'location' => 'json',
                ],
            ],
        ],
    ];

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->container = new ContainerBuilder();
        $extension = new LinkmarkGuzzleExtension();

        $this->container->registerExtension($extension);
        $this->container->loadFromExtension($extension->getAlias());
    }

    /**
     * @covers ::process
     */
    public function testProcess()
    {
        $container = $this->container;

        $provider = new Definition(
            'GuzzleHttp\Command\Guzzle\Description',
            [
                $this->description,
            ]
        );
        $provider->addTag('linkmark_guzzle.service.description');

        $container->setDefinition('acme.guzzle.service', $provider);

        $container->addCompilerPass(new ServicesCompilerPass());
        $container->compile();

        $this->assertTrue($container->has('linkmark_guzzle'));
        $this->assertTrue($container->has('linkmark_guzzle.services.acme.guzzle.service'));
    }

    /**
     * @covers ::process
     */
    public function testProcessWithCustomName()
    {
        $container = $this->container;

        $provider = new Definition(
            'GuzzleHttp\Command\Guzzle\Description',
            [
                $this->description,
            ]
        );
        $provider->addTag(
            'linkmark_guzzle.service.description',
            [
                'name' => 'httpbin',
            ]
        );

        $container->setDefinition('acme.guzzle.service.description', $provider);

        $container->addCompilerPass(new ServicesCompilerPass());
        $container->compile();

        $this->assertTrue($container->has('linkmark_guzzle'));
        $this->assertTrue($container->has('linkmark_guzzle.services.httpbin'));
    }
}
