<?php

namespace Linkmark\GuzzleBundle\Tests\DependencyInjection;

use Linkmark\GuzzleBundle\Tests\AppKernel;
use PHPUnit_Framework_TestCase;

/**
 * @coversDefaultClass \Linkmark\GuzzleBundle\DependencyInjection\LinkmarkGuzzleExtension
 */
class LinkmarkGuzzleExtensionTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Symfony\Component\HttpKernel\Kernel
     */
    private $kernel;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->kernel = new AppKernel('LinkmarkGuzzleExtensionTest', true);
        $this->kernel->boot();
    }

    /**
     * @covers ::load
     */
    public function testService()
    {
        $container = $this->kernel->getContainer();

        $this->assertTrue($container->has('linkmark_guzzle'));
        $this->assertInstanceOf('GuzzleHttp\\ClientInterface', $container->get('linkmark_guzzle'));
    }
}
