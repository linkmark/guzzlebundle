<?php

namespace Linkmark\GuzzleBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class LinkmarkGuzzleExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.xml');

        // Twig extension is loaded only if it's enabled
        if ($config['twig']['enabled']) {
            $loader->load('twig.xml');
        }

        // Load subscribers sections
        $this->loadSubscribers($config['subscribers'], $container);

        // Replace the emitter with a new one because
        // the framework doesn't allow calls on getters
        $guzzle = $container->getDefinition('linkmark_guzzle');

        $config['client']['emitter'] = new Reference('linkmark_guzzle.event.emitter');

        $guzzle->replaceArgument(0, $config['client']);
    }

    /**
     * For each subscriber, if enabled, load services and parameters.
     *
     * @param array            $config    Subscribers section only.
     * @param ContainerBuilder $container Container builder.
     */
    private function loadSubscribers(array $config, ContainerBuilder $container)
    {
        $loader = new Loader\XmlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config/subscribers')
        );

        // Cache is loaded only if it's enabled
        if ($config['cache']['enabled']) {
            $loader->load('cache.xml');
        }

        // Server side cache is loaded only if it's enabled
        if ($config['server_cache']['enabled']) {
            $loader->load('server_cache.xml');
        }

        // Logger is loaded only if it's enabled
        if ($config['log']['enabled']) {
            $loader->load('log.xml');
        }

        // OAuth is loaded only if it's enabled
        if ($config['oauth1']['enabled']) {
            $loader->load('oauth1.xml');

            $this->loadOAuthConfiguration($config['oauth1'], $container);
        }

        // Retry system is loaded only if it's enabled
        if ($config['retry']['enabled']) {
            $loader->load('retry.xml');

            $this->loadRetryConfiguration($config['retry'], $container);
        }
    }

    /**
     * Loads different OAuth configurations.
     *
     * @param array            $config    OAuth section only.
     * @param ContainerBuilder $container Container builder.
     */
    private function loadOAuthConfiguration(array $config, ContainerBuilder $container)
    {
        // required configurations
        $container->setParameter('linkmark_guzzle.subscribers.oauth1.consumer_key', $config['consumer_key']);
        $container->setParameter('linkmark_guzzle.subscribers.oauth1.consumer_secret', $config['consumer_secret']);
        $container->setParameter('linkmark_guzzle.subscribers.oauth1.oauth_version', $config['version']);
        $container->setParameter('linkmark_guzzle.subscribers.oauth1.request_method', $config['request_method']);
        $container->setParameter('linkmark_guzzle.subscribers.oauth1.signature_method', $config['signature_method']);

        // optional configurations
        if ($config['callback']) {
            $container->setParameter('linkmark_guzzle.subscribers.oauth1.callback', $config['callback']);
        }

        if ($config['realm']) {
            $container->setParameter('linkmark_guzzle.subscribers.oauth1.realm', $config['realm']);
        }

        if ($config['token']) {
            $container->setParameter('linkmark_guzzle.subscribers.oauth1.token', $config['token']);
        }

        if ($config['token_secret']) {
            $container->setParameter('linkmark_guzzle.subscribers.oauth1.token_secret', $config['token_secret']);
        }

        if ($config['verifier']) {
            $container->setParameter('linkmark_guzzle.subscribers.oauth1.oauth_verifier', $config['verifier']);
        }
    }

    /**
     * Loads different retry configurations.
     *
     * @param array            $config    Retry section only.
     * @param ContainerBuilder $container Container builder.
     */
    private function loadRetryConfiguration(array $config, ContainerBuilder $container)
    {
        $container->setParameter('linkmark_guzzle.subscribers.retry.delay', $config['delay']);
        $container->setParameter('linkmark_guzzle.subscribers.retry.filter.class', $config['filter']['class']);
        $container->setParameter('linkmark_guzzle.subscribers.retry.filter.method', $config['filter']['method']);
        $container->setParameter('linkmark_guzzle.subscribers.retry.max', $config['max']);
    }
}
